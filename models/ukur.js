'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Ukur extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      Ukur.hasMany(models.Cars, {
        as: 'Cars',
        foreignKey: 'size_id'
      })
    }
  }
  Ukur.init({
    size: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Ukur',
  });
  return Ukur;
};
const { Cars } = require("../../models");

module.exports = {
    list(req, res) {
        Cars.findAll(
            {
                order: ['id']
            }
        )
            .then((cars) => {
                res.render("index", { cars })
                // res.status(200).json(cars)
            })
    }
}